# Arch Linux Setup for Desktop

## Quick Basics

* Basic text editors included are nano, vim, and vi.
* To navigate to different consoles, use <kbd>Alt</kbd>+left/right key.
* Initial login is 'root' with no password.

## SSH (optional)

### Computer for Arch to be installed

Requires a working internet connection, so refer to WiFi Connect below.

Requires a password to be set. Following code sets one for Arch that is currently on the disk it is booted on.

`# passwd`

Start ssh on systemctl.

`# systemctl start sshd`

Get IP address.

`# ip addr`

### Computer to SSH from

Connect to computer and type in password.

`# ssh root@<IP address of computer>`

If terminal is not working as it should be, set the terminal as vt100 as an environmental variable.

`# export TERM=vt100`

## WiFi Connect
 `# wifi-menu`

## WiFi Connect - long winded way

Check network interfaces.

`# iw dev`

Check if wpa supplicant present.

`# wpa_supplicant`

"Connect"

1. Navigate to `\etc\netctl\` as profile needs to be stored in this folder.

2. Create a file containing netctl profile.
```
Description='<WiFi Network>'
Interface=<interface>
Connection=wireless
Security=wpa
IP=dhcp
ESSID='<Network Name>'
Key='WiFi Password'
```

3. To "hide" passphrase.

`# wpa_passphrase <ESSID> <key>` and substitute 'Key' of profile with `\"<psk output>`

4. Start profile.

`# netctl start <profile>`

5. Check if profile working.

`# ping <website,`<i>`e.g.`</i>`archlinux.org>`

## Partition Hard Drive

### Set up partitions

Check hard drive (especially important if trying to set up on a specific hard drive). In my case, I want to set OS up in /dev/md126 which is two SSDs combined with RAID.

`# lsblk`

Use parted to configure partitions.

`# parted /dev/sda`

Set up a new partition table (!!! REMOVES PREVIOUS PARTITIONS AND ACCOMPANYING DATA !!!).
Basic idea is:
- 512MiB for EFI
- 2GiB for swap
- 40GiB for root
- remaining for home/music/whatever

`(parted) mklabel gpt`

Set up boot partition.

`(parted) mkpart ESP fat32 1MiB 513MiB`

`(parted) set 1 boot on`

Set up swap partition.

`(parted) mkpart primary linux-swap 513MiB 2GiB`

Set up root partition.

`(parted) mkpart primary ext4 2GiB 42GiB`

Set up home partition.

`(parted) mkpart primary ext4 42GiB 76%`

Set up Windows partition.

`(parted) mkpart primary NTFS 76% 100%`

Exit.

`(parted) quit`

Check newly partitioned drive.

`# fdisk -l /dev/sda`

### Format Partitions

Inspect newly partitioned drive.

`# fdisk -l /dev/sda`

Format first (EFI) partition into fat32.

`# mkfs.fat -F32 /dev/sda1`

Format third (root) partition into ext4.

`# mkfs.ext4 /dev/md126p3`

Format fourth (home) partition into ext4.

`# mkfs.ext4 /dev/md126p4`

Format second (swap) partition into swap.

`# mkswap /dev/md126p2`

### Mount Newly Formatted Partitions

Confirm newly formatted and partitioned partitions.

`# fdisk -l /dev/md126`

Root

`# mount /dev/md126p3 /mnt`

Home

`# mkdir /mnt/home`

`# mount /dev/md126p4 /mnt/home`

Boot

`# mkdir /mnt/boot`

`# mount /dev/md126p1 /mnt/boot`

Swap

`# swapon /dev/md126p2`

## Installing Arch

Install base.

`pacstrap /mnt base linux linux-firmware vi`

## Some Configurations

Generate fstab.

`# genfstab -p -U /mnt >> /mnt/etc/fstab`

Access /mnt as a root, and stay in root for a bit.

`# arch-chroot /mnt`

Configure mdadm into initramfs

`vi /etc/mkinitcpio.conf`

Include `mdadm_udev` right before `filesystems`

Set Hostname.

`# echo <hostname> > /etc/hostname`

Set timezone by removing current time file and setting symbolic link to right time zone file.

`# rm -f /etc/localtime`

`# ln -s /usr/share/zoneinfo/Canada/Pacific /etc/localtime`

Enable multilib in pacman config file by uncommenting out multilib lines, and enable parallel downloads if wanted.

`# nano /etc/pacman.conf`

```
[multilib]
Include = /etc/pacman.d/mirrorlist
```

Install some basic programs.

`# pacman -Syu --needed sudo openssh base-devel dhcpcd nvim git dialog intel-ucode zsh`

Enable ssh on boot

`# systemctl enable sshd`

Set locales by uncommenting out desired locale.

`# nano /etc/locale.gen`

`en_CA.UTF-8 UTF-8`

`# locale-gen`

Set language variable.

`# nano /etc/locale.conf`

`LANG=en_CA.UTF-8`

Enable networking on boot.

`# systemctl enable dhcpcd`

Make initial ramdisk.

`# mkinitcpio -p linux`

Booting

 `# bootctl install`

Configure boot loader

Replace content of loader.conf with Arch

`# nano /boot/loader/loader.conf`

`default arch`

Add an Arch Configuration file

`# touch /boot/loader/entries/arch.conf`

```
title	Arch Linux
linux	/vmlinuz-linux
initrd  /intel-ucode.img
initrd	/initramfs-linux.img
options	root=/dev/md126p3 rw quiet

```

Set up Users

Set root's password

`# passwd`

Create a user

`# useradd -m -G wheel -s /bin/zsh your_username`

Set the user`s password

`# passwd your_username`

Edit the sudoers with this special vi command

``# visudo`

Find `root ALL=(ALL) ALL` and add `your_username ALL=(ALL) ALL` in next line.

Reboot, remove live USB, and try booting through computer itself.

`poweroff`

If wifi-menu not connecting, but still can find network

Error after `wifi-menu` or `netctl start wlp2s0-ssid`

`Job for netctl@wlan0\x2ssid.service failed. See 'systemctl status netctl@wlan0\x2ssid.service' and 'journalctl -xn' for details.`

Accessing `journalctl -xn`, there should be a line:

`network[2322]: The interface of network profile 'wlan0-ssid' is already up
`

`ip link set wlp2s0 down`

Connect should work now.

If not, navigate to `/etc/netctl/wlp2s0-ssid` and add `ForceConnect=yes` at end of file.

## Install remaining programs

Install packages

`$ pacman -S --needed $(< ~/list_of_programs)`

Reboot and when re-enter tty

`startx`

Start an .xinitrc file

```
[[ -f ~/.Xdefaults ]] && xrdb -merge -I $HOME ~/.Xdefaults
exec i3
```

GUI configuration

`lxappearance`

Start and autostart automatic wifi connection (https://wiki.archlinux.org/index.php/Netctl)

`systemctl start/enable netctl-auto@wlp2s0.service`

Getting cower

'git clone https://aur.archlinux.org/cower.git'
`gpg --recv-keys --keyserver hkp://pgp.mit.edu 1EB2638FF56C0C53`


Getting bitmap fonts

'https://www.github.com/Tecate/bitmap-fonts'
`find /usr/share/fonts/bitmap/ -name fonts.dir | xargs dirname | xargs -I {} xset fp+ {}`

Backup

For native packages: `pacman -Qqn`

For foreign packages (*e.g.* aur packages): `pacman -Qqm`

Git Config

`git config --global user.name "username"`

`git config --global user.email "email"`

`git config --global core.editor "nvim"~`
