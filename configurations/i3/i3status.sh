track_info=""

update_track_info() {
    status=$(playerctl -p cmus status)
    if [[ "$status" = "Paused" ]]; then
        track_info="{\"full_text\":\" $(playerctl -p cmus metadata title)\"},"
    elif [[ "$status" = "Playing" ]]; then
        track_info="{\"full_text\":\" $(playerctl -p cmus metadata title)\"},"
    else
        track_info="{\"full_text\":\"\"},"
    fi
}

i3status -c ~/.config/i3/i3status.conf | (read line && echo $line && read line && echo $line && read line && echo $line && read line && echo $line && while :
do
    update_track_info
    read line
    echo ",[${track_info}${line#,\[}" || exit 1
done)
