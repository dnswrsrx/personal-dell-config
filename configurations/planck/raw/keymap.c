// This is the canonical layout file for the Quantum project. If you want to add another keyboard,
// this is the style you want to emulate.

#include QMK_KEYBOARD_H

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
enum planck_layers {
  _QWERTY,
  _RAISE,
  _LOWER,
  _BRACKETS
};

enum planck_keycodes {
  QWERTY = SAFE_RANGE,
  RAISE,
  LOWER,
  BRACKETS,
  PERM_LOWER,
};

#define LOWER MO(_LOWER)
#define RAISE MO(_RAISE)
#define PERM_LOWER TG(_LOWER)
#define BRACKETS LT(_BRACKETS, KC_LBRC)
#define CTL_ESC CTL_T(KC_ESC)
#define RSHIFT_Q RSFT_T(KC_QUOT)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,--------------------------------------------------------------------------------------.
 * | Tab    |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp  |
 * |--------+------+------+------+------+-------------+------+------+------+------+-------|
 * |Ctrl/Esc| ML/A |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  | Enter |
 * |--------+------+------+------+------+------|------+------+------+------+------+-------|
 * | Shift  |   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |   /  |   '   |
 * |--------+------+------+------+------+------+------+------+------+------+------+-------|
 * | `      | Ctrl | Gui  | Alt  |Raise |      Space  |Lower | [    | ]    | Perm |  \    |
 * `--------------------------------------------------------------------------------------'
 */
[_QWERTY] = LAYOUT_planck_1x2uC(
  KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
  CTL_ESC, KC_A,   KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_ENT,
  KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, RSHIFT_Q,
  KC_GRAVE, KC_LCTL, KC_LGUI, KC_LALT, RAISE,   KC_SPACE, LOWER, BRACKETS, KC_RBRC, PERM_LOWER, KC_BSLS
),

/* Raise
 * .-----------------------------------------------------------------------------------.
 * |   F1 |  F2  |  F3  |  F4  |  F5  |  F6  |  F7  |  F8  |  F9  | F10  |   (  |  )   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |   1  |   2  |   3  |   4  |   5  |  6   |  7   |   8  |   9  |   0  |   -  |  =   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |Shift |      |      |      |      |      |      |      |   ,  |  .   |  /   |  '   |
 * +------+------+------+------+------+------+------+------+------+------+------+------|
 * | Ctrl |      | Gui  | Alt  |      |             |Lower |  [   |  ]   |      | Ctrl |
 * `-----------------------------------------------------------------------------------'
 */
[_RAISE] = LAYOUT_planck_1x2uC(
  KC_F1, KC_F2, KC_F3,   KC_F4, KC_F5,  KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_LPRN, KC_RPRN,
  KC_1, KC_2,   KC_3,   KC_4,   KC_5,   KC_6,   KC_7,   KC_8, KC_9, KC_0, KC_MINS, KC_EQL,
  KC_TRNS, KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,  KC_NO, KC_TRNS,  KC_TRNS, KC_TRNS, RSHIFT_Q,
  KC_LCTL, KC_NO, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO, KC_RCTL
 ),

/* Lower
 * ,-----------------------------------------------------------------------------------.
 * |   `  | Prev | Play | Next |      |      |      | Home |  Up  |  End | Del  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |Ctl/Es| Mute | Vol- | Vol+ |      |      |      | Left | Down | Right|      | Ent  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Shift|   z  |   x  |   c  |   v  |      |      |      |      |      |      | PgUp |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      | Gui  | Alt  |      |             |      |      |      |      | PgDn |
 * `-----------------------------------------------------------------------------------'
 */
[_LOWER] = LAYOUT_planck_1x2uC(
  KC_GRAVE, KC_MPRV, KC_MPLY, KC_MNXT, KC_NO, KC_NO, KC_NO, KC_HOME, KC_UP, KC_END, KC_DEL, KC_BSPC,
  CTL_ESC, KC_MUTE, KC_VOLD, KC_VOLU, KC_NO, KC_NO,  KC_NO, KC_LEFT, KC_DOWN, KC_RIGHT, KC_NO, KC_TRNS,
  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_PGUP,
  KC_NO, KC_NO, KC_TRNS, KC_TRNS, KC_NO, KC_NO, KC_TRNS, KC_NO, KC_NO, PERM_LOWER, KC_PGDN
),

/* Perm lower
 * ,-----------------------------------------------------------------------------------.
 * |      |      |      |      |      |      |      |      |      |      |   [  |   ]  |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Shift|      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */

[_BRACKETS] = LAYOUT_planck_1x2uC(
  KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LBRC, KC_RBRC,
  KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
  KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,
  KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO
),

};

bool music_mask_user(uint16_t keycode) {
  return false;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  // If console is enabled, it will print the matrix position and status of each key pressed
  #ifdef CONSOLE_ENABLE
    uprintf("KL: kc: 0x%04X, col: %u, row: %u, pressed: %b, time: %u, interrupt: %b, count: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed, record->event.time, record->tap.interrupted, record->tap.count);
  #endif 
  return true;
}
