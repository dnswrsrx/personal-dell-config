#pragma once

#define TAPPING_TERM 100

#ifdef AUDIO_ENABLE
    #define AUDIO_INIT_DELAY
    #define STARTUP_SONG SONG(VIOLIN_SOUND)
    #define TEMPO_DEFAULT 100
#endif
