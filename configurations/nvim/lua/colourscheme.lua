-- Make matching braces red
vim.api.nvim_set_hl(0, 'MatchParen', { bold=true, underline=true })

-- Line Numbering
vim.api.nvim_set_hl(0, 'Comment', { ctermfg=246, fg='#949494' })

vim.api.nvim_set_hl(0, 'Visual', { link='TabLineSel' })
vim.api.nvim_set_hl(0, 'NormalFloat', { link='Pmenu' })

-- Unbold identifier, usually constants or language keywords
vim.api.nvim_set_hl(0, 'Identifier', { ctermfg=14, bold=false })
vim.api.nvim_set_hl(0, 'Statement', { bold=false })

vim.api.nvim_set_hl(0, 'LineNr', { link='Comment' })
vim.api.nvim_set_hl(0, 'TabLine', { link='Comment' })

vim.api.nvim_set_hl(0, 'TabLineSel', { ctermbg=240, bg='#585858' })
vim.api.nvim_set_hl(0, 'TabLineFill', { ctermbg=nil, bg=nil })

vim.api.nvim_set_hl(0, 'Pmenu', { ctermbg=237, bg='#3a3a3a', ctermfg=nil, fg=nil })
vim.api.nvim_set_hl(0, 'PmenuSel', { link='TabLineSel' })

vim.api.nvim_set_hl(0, 'QuickFixLine', { link='TabLineSel' })

vim.api.nvim_set_hl(0, 'Todo', { ctermbg=214, bg='#ffaf00' })

vim.api.nvim_set_hl(0, 'Folded', { link='Comment' })

vim.api.nvim_set_hl(0, 'SpellBad', { ctermfg=15, ctermbg=9, undercurl=true, sp='red' })
vim.api.nvim_set_hl(0, 'SpellCap', { ctermfg=15, ctermbg=12, undercurl=true, sp='blue' })
vim.api.nvim_set_hl(0, 'SpellRare', { ctermfg=15, ctermbg=13, undercurl=true, sp='magenta' })
vim.api.nvim_set_hl(0, 'SpellLocal', { ctermfg=15, ctermbg=14, undercurl=true, sp='cyan' })

-- Clear out sign column highlight for Gitsigns and LSP
vim.api.nvim_set_hl(0, 'SignColumn', { ctermfg=14, fg='#00ffff' })

vim.api.nvim_set_hl(0, 'WinSeparator', { link='Comment' })
