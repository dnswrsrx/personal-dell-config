vim.opt.title = true

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.linebreak = true

vim.opt.scrolloff = 2

vim.opt.swapfile = false

vim.opt.ttimeoutlen = 0

vim.opt.showmode = false

-- This allows us to ignore case in commands (Ex mode)
-- and brings up completion case-insensitive
vim.opt.ignorecase = true

-- persistent undo
vim.opt.undofile = true
vim.opt.undodir = vim.fn.expand('~/.undodir')

-- Lets us copy and paste to system clipboard
vim.opt.clipboard:append('unnamedplus')

-- Style of completion in command, complete if only one, otherwise show list without completing
vim.opt.wildmode:prepend('longest:full')

-- Default indentation settings
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Default split direction
vim.opt.splitright = true
vim.opt.splitbelow = true
--
-- Remove ~ as end of buffer filler
vim.opt.fillchars='eob: '

-- Active highlight cursor line so that the current line number can be highlighted
-- Will disable the actual cursor line highlighting however.
vim.opt.cursorline = false

-- Make diff split vertical by default
vim.opt.diffopt:append('vertical')

vim.opt.termguicolors= false

vim.g.python3_host_prog = '/usr/bin/python'
