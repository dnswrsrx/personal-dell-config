return {
    'ibhagwan/fzf-lua',
    event = 'VeryLazy',
    config = function()
        local actions = require "fzf-lua.actions"
        local fzflua = require "fzf-lua"

        fzflua.setup({
            winopts = {
                hls = {
                    -- Since we've essentially removed CursorLine
                    -- Visual is a decent "highlighter"
                    cursorline = 'Visual',
                },
                preview = {
                    wrap = 'wrap',
                    horizontal = 'right:70%',
                    title = false,
                    scrollbar = false,
                    winopts = {
                        -- To just highlight line and not line number
                        cursorlineopt = 'line',
                    }
                }
            },
            keymap = {
                builtin = {
                    ["<F1>"] = "toggle-help",
                    ["<F3>"] = "toggle-preview-wrap",
                    ["<c-f>"] = "toggle-fullscreen",
                    ["<c-h>"] = "toggle-preview",
                    ['<c-d>'] = 'preview-page-down',
                    ['<c-u>'] = 'preview-page-up',
                },
                fzf = {
                    ["shift-down"] = "half-page-down",
                    ["shift-up"] = "half-page-up",
                    ["ctrl-a"] = "beginning-of-line",
                    ["ctrl-e"] = "end-of-line",
                    ["ctrl-l"] = "toggle-all"
                }
            },
            actions = {
                files = {
                    ["default"] = actions.file_edit_or_qf,
                    ["ctrl-s"] = actions.file_split,
                    ["ctrl-v"] = actions.file_vsplit,
                    ["ctrl-t"] = actions.file_tabedit,
                    ["ctrl-q"] = actions.file_sel_to_qf
                }
            }
        })

        map('', '<c-b>', fzflua.buffers)
        map('', '<c-n>', fzflua.files)
        map('', '<c-p>', fzflua.live_grep_resume)
        map('v', '<c-\\>', fzflua.grep_visual)
        map('n', '<c-\\>w', fzflua.grep_cword)
        map('n', '<c-\\>W', fzflua.grep_cWORD)

        map('', '<leader>rf', fzflua.lsp_references)
    end
}


