return {
    {
        'folke/ts-comments.nvim',
        config = true,
        event = 'VeryLazy',
        init = function()
            remap('n', ',', 'gcc')
            remap({'v', 'o'}, ',', 'gc')
        end
    }
}
