return {
    'airblade/vim-rooter',
    config = function()
        vim.g.rooter_silent_chdir = true
        -- Use local cd, so chdir applies to the current pane only
        vim.g.rooter_cd_cmd = 'lcd'
        vim.g.rooter_patterns = {'=src', 'venv', 'requirements.txt', '=nvim', 'package.json', '.git', 'Makefile'}

        vim.api.nvim_create_user_command(
            'RooterUn',
            function()
                if vim.startswith(vim.api.nvim_buf_get_name(0), 'fugitive://') then
                    vim.print('Cannot un-Rooter in Fugitive')
                else
                    vim.cmd('RooterToggle')
                    vim.cmd.lcd(vim.fs.dirname(vim.api.nvim_buf_get_name(0)))
                end
            end,
            {
                desc = 'lcd to the parent of current file'
            }
        )
    end
}
