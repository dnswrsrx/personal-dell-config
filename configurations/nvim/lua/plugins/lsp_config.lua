return {
    {
        'RRethy/vim-illuminate',
        init = function()
            vim.api.nvim_set_hl(0, 'illuminatedWordText', { ctermbg=238, bg='#444444' })
            vim.api.nvim_set_hl(0, 'illuminatedWordRead', { link='illuminatedWordText' })
            vim.api.nvim_set_hl(0, 'illuminatedWordWrite', { link='illuminatedWordText' })
        end,
        config = function()
            require('illuminate').configure({
                providers = {
                    'regex'
                },
                filetypes_denylist = {
                    'NvimTree',
                    'aerial',
                    'qf',
                }
            })
        end
    },
    { 'williamboman/mason.nvim', config = true },
    { 'neovim/nvim-lspconfig', lazy = true },
    {
        'williamboman/mason-lspconfig.nvim',
        dependencies = {
            'neovim/nvim-lspconfig',
            'hrsh7th/cmp-nvim-lsp',
        },
        init = function()
            vim.api.nvim_set_hl(0, 'LspSignatureActiveParameter', { bold=true, underline=true })

            vim.diagnostic.config({
                virtual_text = false,
                float = {
                    header = '',
                    scope = 'cursor',
                    severity_sort = true,
                    focusable = false
                }
            })
        end,
        config = function()
            local servers = {'basedpyright', 'ts_ls', 'cssls', 'html', 'jsonls', 'lexical', 'lua_ls', 'tinymist'}

            local mason_lspconfig = require('mason-lspconfig')

            mason_lspconfig.setup({
                ensure_installed = servers
            })

            mason_lspconfig.setup_handlers({
                function (server)
                    local opts = {
                        on_attach = function(client, bufnr)
                            client.server_capabilities.semanticTokensProvider = nil
                        end,
                        capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
                    }
                    if server == 'tinymist' then
                        opts['settings'] = {exportPdf='never'}
                    end
                    require('lspconfig')[server].setup(opts)
                end
            })

            map('n', '<leader>dd', vim.lsp.buf.definition)
            map('n', '<leader>ds', ':sp<cr><cmd> lua vim.lsp.buf.definition()<cr>')
            map('n', '<leader>dv', ':vs<cr><cmd> lua vim.lsp.buf.definition()<cr>')

            -- Cursor position only updated after save, from restore_view plugin
            -- Opening file in new tab might not have the current cursor position
            -- Hack here to place a mark on current cursor position
            -- Once file opened in new tab, go to the mark and call definition
            map('n', '<leader>dt', 'mm :tabe %<cr>`m<cmd> lua vim.lsp.buf.definition()<cr>')
            map('n', '<leader>y', vim.lsp.buf.type_definition)
            map('n', '<leader>c', vim.lsp.buf.hover)
            map('n', '<leader>re', vim.lsp.buf.rename)

            map('n', '<leader>dn', vim.diagnostic.goto_next)
            map('n', '<leader>dp', vim.diagnostic.goto_prev)
            map('n', '<leader>da', vim.diagnostic.open_float)
            map('n', '<leader>=', vim.lsp.buf.format)

            -- View references in quickfix
            map('n', '<leader>rq', vim.lsp.buf.references)

            map('i', '<c-\\>', vim.lsp.buf.signature_help)
        end
    },

    { 'honza/vim-snippets', lazy=true },
    {
        'dcampos/nvim-snippy',
        dependencies = {
            'honza/vim-snippets'
        },
        opts = {
            mappings = {
                [{'i', 's'}] = {
                    ['<tab>'] = 'expand_or_advance',
                    ['<s-tab>'] = 'previous',
                    ['<c-j>'] = 'expand_or_advance',
                    ['<c-k>'] = 'previous',
                }
            },
            scopes = {
                typescriptreact = { 'html' }
            }
        }
    },

    { 'hrsh7th/cmp-nvim-lsp', lazy=true },
    { 'lukas-reineke/cmp-under-comparator', lazy=true },
    { 'hrsh7th/cmp-buffer', lazy=true },
    { 'hrsh7th/cmp-path', lazy=true },
    { 'hrsh7th/cmp-cmdline', lazy=true },
    { 'dcampos/cmp-snippy', lazy=true },
    {
        'hrsh7th/nvim-cmp',
        event = {'InsertEnter', 'CmdlineEnter'},
        dependencies = {
            'hrsh7th/cmp-buffer',
            'hrsh7th/cmp-path',
            'hrsh7th/cmp-cmdline',
            'lukas-reineke/cmp-under-comparator',
            'dcampos/cmp-snippy',
        },
        config = function()
            local cmp = require('cmp')
            local snippy = require('snippy')

            -- Better handling of snippets
            -- https://github.com/hrsh7th/nvim-cmp/discussions/1685
            local forward_map = function(fallback)
                if cmp.visible() then
                    if snippy.can_expand_or_advance() then
                        cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
                    else
                        cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
                    end
                else
                    fallback()
                end
            end

            local backward_map = function(fallback)
                if cmp.visible() then
                    if snippy.can_expand_or_advance() then
                        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
                    else
                        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
                    end
                else
                    fallback()
                end
            end

            cmp.setup({
                snippet = {
                    expand = function(args)
                        snippy.expand_snippet(args.body)
                    end
                },
                window = {
                    -- completion = cmp.config.window.bordered(),
                    -- documentation = cmp.config.window.bordered(),
                },
                mapping = cmp.mapping.preset.insert({
                    ['<c-j>'] = forward_map,
                    ['<c-k>'] = backward_map,
                    ['<tab>'] = forward_map,
                    ['<s-tab>'] = backward_map,
                    ['<c-n>'] = forward_map,
                    ['<c-p>'] = backward_map,

                    ['<cr>'] = function(fallback)
                        if cmp.visible() then
                            if not cmp.confirm() then
                                fallback()
                            end
                        else
                            fallback()
                        end
                    end,

                    ['<c-d>'] = cmp.mapping.scroll_docs(4),
                    ['<c-u>'] = cmp.mapping.scroll_docs(-4),
                    ['<c-space>'] = cmp.mapping.complete(),
                    ['<c-e>'] = cmp.mapping.abort(),
                }),
                sources = cmp.config.sources(
                {
                    { name = 'nvim_lsp' },
                    { name = 'snippy' },
                    { name = 'buffer' },
                    { name = 'path' }
                }
                ),
                sorting = {
                    comparators = {
                        cmp.config.compare.offset,
                        cmp.config.compare.exact,
                        cmp.config.compare.score,
                        cmp.config.compare.recently_used,
                        require("cmp-under-comparator").under,
                        cmp.config.compare.kind,
                    }
                }
            })

            cmp.setup.cmdline('/', {
                mapping = cmp.mapping.preset.cmdline(),
                sources = {
                    { name = 'buffer' }
                }
            })

            cmp.setup.cmdline(':', {
                mapping = cmp.mapping.preset.cmdline(),
                sources = cmp.config.sources(
                {
                    { name = 'path' },
                    { name = 'cmdline' }
                }
                )
            })

            map('i', '<esc>', [[pumvisible() ? "\<c-e><esc>" : "\<Esc>"]], {expr=true})
            map('i', '<c-c>', [[pumvisible() ? "\<c-e><c-c>" : "\<c-c>"]], {expr=true})
            map('i', '<bs>', [[pumvisible() ? "\<c-e><bs>" : "\<bs>"]], {expr=true})
            map('i', '<cr>', [[pumvisible() ? (complete_info().selected == -1 ? "\<c-e><cr>" : "\<c-y>") : "\<cr>"]], {expr=true})

            map('i', '<tab>', [[pumvisible() ? "\<c-n>" : "\<Tab>"]], {expr=true})
            map('i', '<s-tab>', [[pumvisible() ? "\<c-p>" : "\<s-tab>"]], {expr=true})

            map('i', '<c-j>', [[pumvisible() ? "\<c-n>" : "\<c-j>"]], {expr=true})
            map('i', '<c-k>', [[pumvisible() ? "\<c-p>" : "\<c-k>"]], {expr=true})
        end
    }
}
