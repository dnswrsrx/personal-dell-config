require('lazynvim_init')

require('_utils')

require('options')
require('keymaps')
require('colourscheme')
require('autocommands')

require('tabline')
require('trailing_spaces')

require('lazy').setup(
    'plugins',
    {
        change_detection = {
            notify = false
        },
    }
)
